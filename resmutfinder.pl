#!/usr/bin/perl

# --------------------------------------------------------------------
# %% Setting up %%
#

use strict;
use Getopt::Long qw(:config no_ignore_case no_auto_abbrev pass_through);

use constant PROGRAM_NAME            => 'PointFinder';
use constant PROGRAM_NAME_LONG       => 'Findes chromosomal point mutations in genes for a sequence or genome';
use constant VERSION                 => '3.0';

my ($Help, $species, $InFile, $dir, $patient,$unknown_mutations, $output_path, $database, $BLASTN);

&GetOptions (
   "s=s"     => \$species,
   "i=s"     => \$InFile,
   "u=s"     => \$unknown_mutations,
   "o=s"     => \$dir,
   "b=s"     => \$BLASTN,
   "d=s"     => \$database,
   "h|help"  => \$Help               # Prints the help text
);


if (defined $Help || not defined $species || not defined $InFile || not defined $database ) {
   print_help();
   exit;
}

if (defined not $BLASTN) {
   $BLASTN = "blastn"
}
if (not defined $dir) {
  $dir = ".";
}
my %Temp = @ARGV;
@ARGV{keys %Temp} = values %Temp;

# Defining path to overview file
my $DB   = "$database/$species";
my $drug = "$database/$species/resistens-overview.txt";

# --------------------------------------------------------------------
# Printing info
#---------------------------------------------------------------------

#
## AVAILABLE species ##
my %argfProfiles=();
##################### species #################
open(IN, '<', "$database/config") or die "Error: $!\n";
while (defined(my $line = <IN>)) {
   next if $line =~ m/^#/;  # discard comments
   my @tmp = split("\t", $line);
   $argfProfiles{$tmp[0]} = $tmp[1];
}
close IN;

#### FINDING THE RUNROOT ## Added to print textresults
my @tmpArrRunroot = split(/inputs/, $InFile);

# -----------------------
#Getting the data and positions of the know mutations from resistens file
my %known_mutations = &mutations();

#Options
my $threshold = "50";
my $frame_shift = "on";

my @path_infile = split(",",$InFile);

## Variables ##
#my %GENES; #Saves 
my @RESULTS_AND_SETTINGS_ARRAY; #will contain the typing results some setting and the hash with the results for each gene
my @GENE_RESULTS_ARRAY; #will contain the typing results some setting and the hash with the results for each gene
my %GENE_RESULTS_HASH;
my %multi_print;
my %single_print;
my %silent_mut;
my %non_silent_mut;

# Making list of genes - DNA
my @genes;
open(IN,'<'. "$DB/genes.txt") or die "Can't open gene file";
while (defined(my $line = <IN>)) {
   chomp($line);
   push(@genes, $line)
}
close IN;
# Making list of genes - rRNA
my @RNAgenes;
open(IN2, '<', "$DB/RNA_genes.txt") or die "Can't open RNA gene file";
while (defined(my $line = <IN2>)) {
   chomp($line);
   push(@RNAgenes, $line)
}
close IN2;


######################## MAIN PROGRAM ##################################
my $PATH = "";
foreach my $path_infile (@path_infile) {
   my @tmp = split("/",$path_infile);
   $patient = $tmp[-1];
   #Tmp dir for blast
   $PATH = "$dir/tmp";
   mkdir($PATH);
   my %GENES = &blast($path_infile,$PATH);
   #my %RNAGENES = &blast($path_infile,$PATH);
   
   #Finding the genes of interest
   foreach my $gene (keys %GENES) {
   my ($slen, $all_end, $all_start, $perc_cov);
      my $not_aligned = 0;
      my (@endpos, @startpos, @mismatch_pos, @qseqs, @sseqs, @slens);
      my $i = 0;
        
      #Finding coverage
      foreach my $hit (keys %{$GENES{$gene}}) {
         $slen = $GENES{$gene}{$hit}{"slen"} if $i == 0;
         my $sstart = $GENES{$gene}{$hit}{"spos_start"};
         my $send = $GENES{$gene}{$hit}{"spos_end"};
         my $alen = $GENES{$gene}{$hit}{"len"};
         my $gaps = $GENES{$gene}{$hit}{"gaps"};
         my $qseq = $GENES{$gene}{$hit}{"qseq"};              
         my $sseq = $GENES{$gene}{$hit}{"sseq"};
            
         #Newlines are removed from the subject sequence
         $sseq =~ s/\n//g;
         
         #If the alignment is reversed the start position in the subject sequence is now the end posistion vice virsa
         if ($sstart > $send) {
            my $tmp = $sstart;
            $sstart = $send;
            $send = $tmp;
                
            #reversing and compliment the subject and the query as well
            $qseq =~ tr/ATCGatcg/TAGCTAGC/;
            $qseq = reverse($qseq);
            $sseq =~ tr/ATCGatcg/TAGCTAGC/;
            $sseq = reverse($sseq);
         }
            
         #If the 100 % coverage in one alignment the loop ends otherwise the positions and sequences are saved
         if ($alen - $gaps == $slen and $send - $sstart + 1 == $slen) {
            $GENES{$gene}{'found'} = "yes";
            $perc_cov = "100";
            $GENES{$gene}{'perc_cov'} = $perc_cov;
            $GENES{$gene}{'q_sequence'} = $qseq;
            $GENES{$gene}{'s_sequence'} = $sseq;
            $GENES{$gene}{'send'} = $send;
            $GENES{$gene}{'sstart'} = $sstart;
            $GENES{$gene}{'slen'} = $slen;
            last;
         }
         else {
            push (@startpos, "$sstart.$i");
            push (@endpos, $send);
            push (@qseqs, $qseq);
            push (@sseqs, $sseq);
            push (@slens, $slen);
         }
         $i++;
      }
        
      #Getting the coverage and the covered part of the gene if the gene is not covered by only one hit
      unless (exists($GENES{$gene}{'found'})) {
         my $i = 0;
         my $final_gene;
         my $subject_gene;
         foreach my $pos (sort{$a <=> $b} @startpos) {
            my @no = split('\.', $pos);
                
            my ($send, $sstart);
            if ($i == 0) {
               $all_end = $endpos[$no[1]];
               $all_start = $no[0];
               $final_gene = $qseqs[$no[1]];
               $subject_gene = $sseqs[$no[1]];
               $slen = $slens[$no[1]];
            }
            else {
               $send = $endpos[$no[1]];
               $sstart = $no[0];
               my $min_start = $all_end + 1;
               if ($sstart <= $min_start and $send >= $all_end) {
                  my $overlap = $all_end - $sstart + 1;
                  $all_end = $send;
                        
                  #Find out if the overlaps are identical 
                  if ($overlap > 0) {
                     my ($old_gap, $new_gap, $old_gap_sub, $new_gap_sub) = (0,0,0,0);
                     my $new_seq = substr($qseqs[$no[1]], 0, $overlap);
                     my $old_seq = substr($final_gene, length($final_gene) - $overlap, $overlap);
                     my $old_sub_seq = substr($subject_gene, length($subject_gene) - $overlap, $overlap);
                     my $new_sub_seq = substr($sseqs[$no[1]], 0, $overlap);
                           
                     #Finding out if there is gaps in the overlap sequences
                     $new_gap = $new_seq =~ tr/-/-/;
                     $new_gap_sub = $new_sub_seq =~ tr/-/-/;
                     $old_gap = $old_seq =~ tr/-/-/;
                     $old_gap_sub = $old_sub_seq =~ tr/-/-/;
                           
                     #Adding the two seqences depending on how they look
                     if ($new_seq eq $old_seq and $new_sub_seq eq $old_sub_seq) {
                        $final_gene .= substr($qseqs[$no[1]], $overlap);
                        $subject_gene .= substr($sseqs[$no[1]], $overlap);
                     }
                     else {
                        #If there are gaps in the new sequences the old ones are used
                        if ($new_gap != 0 or $new_gap_sub != 0) {
                           #If there are more gaps in the subject sequence the overlap is extended with the no of gaps
                           if ($new_gap_sub > $new_gap) {
                              $final_gene .= substr($qseqs[$no[1]], $overlap + $new_gap_sub);
                              $subject_gene .= substr($sseqs[$no[1]], $overlap + $new_gap_sub);
                           }
                           else {
                              $final_gene .= substr($qseqs[$no[1]], $overlap);
                              $subject_gene .= substr($sseqs[$no[1]], $overlap);
                           }
                           $GENES{$gene}{'overlap_warning'} = 1;
                        }
                        #If there are gaps in the old seqences the new ones are used
                        elsif ($old_gap != 0 or $old_gap_sub != 0) {
                           my ($new_final_gene, $new_subjet_gene);
                                 
                           #If there are more gaps in the subject sequence the overlap is extended with the no of gaps
                           if ($old_gap_sub > $old_gap) {
                              $new_final_gene = substr($final_gene, 0, length($final_gene) - $overlap - $old_gap_sub - 1);
                              $new_subjet_gene = substr($subject_gene, 0, length($subject_gene) - $overlap - $old_gap_sub - 1);
                           }
                           else {
                              $new_final_gene = substr($final_gene, 0, length($final_gene) - $overlap - 1);
                              $new_subjet_gene = substr($subject_gene, 0, length($subject_gene) - $overlap - 1);
                           }
                                    
                           $new_final_gene .= $qseqs[$no[1]];
                           $new_subjet_gene .= $sseqs[$no[1]];
                                    
                           $final_gene = $new_final_gene;
                           $subject_gene = $new_subjet_gene;
                                    
                           $GENES{$gene}{'overlap_warning'} = 1;
                        }
                        #If there are no gaps in the sequences the old one is used and a warning is set
                        else {
                           $final_gene .= substr($qseqs[$no[1]], $overlap);
                           $subject_gene .= substr($sseqs[$no[1]], $overlap);
                           $GENES{$gene}{'overlap_warning'} = 2;
                        }
                     }
                  }
                  #If there is no overlap the sequence is just added
                  else {
                     $final_gene .= $qseqs[$no[1]];
                     $subject_gene .= $sseqs[$no[1]];
                  }
               }
               elsif ($sstart <= $min_start and $send <= $all_end) {
                  my $overlap = $send - $sstart + 1;
                  my $start_in_seq = $sstart - $all_start;
                  my $new_seq = $qseqs[$no[1]];
                  my $new_sub_seq = $sseqs[$no[1]];
                  my $old_seq = substr($final_gene, $start_in_seq, $overlap);
                  my $old_sub_seq = substr($subject_gene, $start_in_seq, $overlap);
                        
                  #If the overlaps are not identical it is desided which one to use
                  if ($new_seq ne $old_seq and $new_sub_seq ne $old_sub_seq) {
                     my ($old_gap, $new_gap, $old_gap_sub, $new_gap_sub) = (0,0,0,0);
                     #Finding out if there is gaps in the overlap sequences
                     $new_gap = $new_seq =~ tr/-/-/;
                     $new_gap_sub = $new_sub_seq =~ tr/-/-/;
                     $old_gap = $old_seq =~ tr/-/-/;
                     $old_gap_sub = $old_sub_seq =~ tr/-/-/;
                            
                     #If there are gaps in the old seqences the new ones are used
                     if ($old_gap != 0 or $old_gap_sub != 0) {
                        my $length_before = $sstart - $all_start + 1;
                        my ($new_final_gene, $new_subjet_gene);
                                
                        #Adding the start
                        if (($length_before - 1) != 0) {
                           $new_final_gene = substr($final_gene, 0, $length_before - 1);
                           $new_subjet_gene = substr($subject_gene, 0, $length_before - 1);
                                    
                           #Adding the overlap
                           $new_final_gene .= $qseqs[$no[1]];
                           $new_subjet_gene .= $sseqs[$no[1]];
                        }
                        else {
                           #Starting at the overlap
                           $new_final_gene = $qseqs[$no[1]];
                           $new_subjet_gene = $sseqs[$no[1]];
                        }
                                
                        #Adding the part after the overlap. If there are more gaps in the subject sequence the overlap is extended with the no of gaps
                        if ($old_gap_sub > $old_gap) {
                           $new_final_gene .= substr($final_gene, $length_before + $overlap + 1 + $old_gap_sub);
                           $new_subjet_gene .= substr($subject_gene, $length_before + $overlap + 1 + $old_gap_sub);
                        }
                        else {
                           $new_final_gene .= substr($final_gene, $length_before + $overlap + 1);
                           $new_subjet_gene .= substr($subject_gene, $length_before + $overlap + 1);
                        }
                                
                        $final_gene = $new_final_gene;
                        $subject_gene = $new_subjet_gene;
                                
                        $GENES{$gene}{'overlap_warning'} = 1;
                     }
                     elsif ($new_gap != 0 or $new_gap_sub != 0) {
                        #The old ones are used and a warning is set
                        $GENES{$gene}{'overlap_warning'} = 1;
                     }
                     else {
                        #If there are no gaps in the sequences the old one is used and a warning is set
                        $GENES{$gene}{'overlap_warning'} = 2;
                     }
                  }
               }
               else {
                  #If there is a space not covered between to hits the number of bases not covered is counted and added as N in the final gene sequence
                  my $tmp_not_aligned = $sstart - $all_end - 1;
                  $not_aligned += $tmp_not_aligned;
                  $all_end = $send;
                        
                  for (my $c = 0; $c < $tmp_not_aligned; $c++) {
                     $final_gene .= "N";
                     $subject_gene .= "N";
                  }
                        
                  $final_gene .= $qseqs[$no[1]];
                  $subject_gene .= $sseqs[$no[1]];
               }
            }
            $i++;
         }
         #Calculating the total coverage and the percent coverage of the gen
         my $cov_len = $all_end - $all_start - $not_aligned + 1;
         if ($slen == 0) {
            $perc_cov = 0;
         }
         else {
            $perc_cov = ($cov_len / $slen) * 100;
         }
         #Finding out if there is any gaps in the final sequence
         my $q_gap = $final_gene =~ tr/-/-/;
         my $s_gap = $subject_gene =~ tr/-/-/;
            
         #If there is an equally amount of gaps in subject and query they are removed to see the changes in the small frame shift and number of gaps are set to 0.
         if ($q_gap eq $s_gap) {
            $final_gene =~ s/-//g;
            $subject_gene  =~ s/-//g;
            $q_gap = 0;
            $s_gap = 0;
         }
            
         #Saving the result
         if ($perc_cov == 100) {
            $GENES{$gene}{'found'} = 'yes';
            $GENES{$gene}{'perc_cov'} = $perc_cov;
            $GENES{$gene}{'q_sequence'} = $final_gene;
            $GENES{$gene}{'s_sequence'} = $subject_gene;
            $GENES{$gene}{'send'} = $all_end;
            $GENES{$gene}{'sstart'} = $all_start;
            $GENES{$gene}{'slen'} = $slen;
         }
         elsif ($perc_cov >= $threshold) {
            $GENES{$gene}{'found'} = "partial";
            $GENES{$gene}{'perc_cov'} = $perc_cov;
            $GENES{$gene}{'q_sequence'} = $final_gene;
            $GENES{$gene}{'s_sequence'} = $subject_gene;
            $GENES{$gene}{'send'} = $all_end;
            $GENES{$gene}{'sstart'} = $all_start;
            $GENES{$gene}{'slen'} = $slen;
                
            #If there is spaces in the gene not covered a warning is set
            $GENES{$gene}{'cov_warning'} = 1 if $not_aligned != 0;
         }
         else {
            $GENES{$gene}{'found'} = "no";
            $GENES{$gene}{'perc_cov'} = $perc_cov;
         }
            
         #Setting the wraning if frame shifts are kept in the sequence and a frame shift orcurred
         if ($q_gap % 3 != 0 or $s_gap % 3 != 0) {
            $GENES{$gene}{'frame_warning'} = 1 if ($q_gap % 3 != 0 or $s_gap % 3 != 0);
                
            #If genes with frame shift is set to not be included the gene is then not found
            $GENES{$gene}{'found'} = 'no' if ($frame_shift eq "off");
         }
      }
   } #END foreach my $gene (keys %genes) 

   #Finding mutations if the gene is found
   foreach my $gene (keys %GENES) {
      #print "GENE: $gene\n";
      if ($GENES{$gene}{'found'} ne 'no') {
         #print "ne no -> $gene\n";
         my ($slen, $all_end, $all_start, $perc_cov);
         my (@mismatch_pos, @mismatch_codon, @original_codon, @resistence_codon);
         
         #Getting the mismatches/mutations positions from query gene compared to the reference/subject gene
         my $qseq = $GENES{$gene}{'q_sequence'};
         my $sseq = $GENES{$gene}{'s_sequence'};
         my $send = $GENES{$gene}{'send'};
         my $sstart = $GENES{$gene}{'sstart'};
         $slen = $GENES{$gene}{'slen'};
         #print "$sstart\n";
            
         # rRNA gene - check for mutations, going through the gene, position by position checking if subject is the same as the query
         if (grep { $_ eq $gene} @RNAgenes)  {
            if ($gene =~ m/promoter_size_(\d+)bp$/i) {
               my $size = $1;
               my $DNA_pos = -$size + 1;
               for (my $i = 0; $i <= length($qseq); $i++){
                  #print("$DNA_pos\n");
                  my $bases_s = substr($sseq, $i, 1);
                  my $bases_q = substr($qseq, $i, 1);
                     
                  if ($bases_s ne $bases_q) {
                     #print "$gene $DNA_pos, $bases_q, $bases_s\n";
                     push(@mismatch_pos, ($DNA_pos));
                     push(@mismatch_codon, $bases_q,);
                     push(@original_codon, $bases_s);
                  }
                  $DNA_pos++;
                  if ($DNA_pos == 0) {
                     $DNA_pos++;
                  }
               }
            }
            else {
               my $DNA_pos = $sstart;
               for (my $i = 0; $i < length($qseq); $i ++){
                  my $bases_s = substr($sseq, $i, 1);
                  my $bases_q = substr($qseq, $i, 1);
                     
                  if ($bases_s ne $bases_q) {
                     #print "$gene $DNA_pos, $bases_q, $bases_s\n";
                     push(@mismatch_pos, $DNA_pos);
                     push(@mismatch_codon, $bases_q,);
                     push(@original_codon, $bases_s);
                  }
                  $DNA_pos++;
               }
            }
         }
         else {
            #If the start position is not the start of a codon the first possible codon position is found
            my $codon_start = 0;
            my $codon_pos = 0;
            if ($sstart != 1 and ($sstart - 1) % 3 != 0) {
               my $tmp_sstart = $sstart;
               for (my $i = 0; $i < 3; $i ++) {
                  $tmp_sstart++;
                  $codon_start++;
                  last if ($tmp_sstart - 1) % 3 == 0;
               }
            }
            
            #Going through the gene  codon by codon checking if the condon in the subject is not the same as in the query
            for (my $i = $codon_start; $i < length($qseq); $i += 3) {
               my $bases_s = substr($sseq, $i, 3);
               my $bases_q = substr($qseq, $i, 3);
                  
               if ($bases_s ne $bases_q) {
                  my $pos = $codon_pos + ($sstart + $codon_start + 2)/3;
                  #if ($bases_s eq "---") {
                  #  $codon_pos--;
                  #}
                  
                  #print "$pos, $bases_q, $bases_s\n";
                  push(@mismatch_pos, $codon_pos + ($sstart + $codon_start + 2)/3);
                  push(@mismatch_codon, $bases_q,);
                  push(@original_codon, $bases_s);
               }
               $codon_pos++;
            }
         }
            
         #Finding out if the mutation positions matches known positions for resistance
         my $j = 0;
         my (@known_pos,@known_codon, @unkmown_pos, @unknown_codon, @ori_codon, @res_codon, @multi_mut_pos, @multi_mut_codon, @multi_mut_all, @multi_ori_codon, @multi_res_codon);
         foreach my $pos (@mismatch_pos) {
            if (exists $known_mutations{$gene}{$pos} and exists $known_mutations{$gene}{$pos}{"no_mut"}) {
               push(@multi_mut_pos, $pos);
               push(@multi_mut_codon, $mismatch_codon[$j]);
               push(@multi_ori_codon, $original_codon[$j]);
               push(@multi_res_codon, $known_mutations{$gene}{$pos}{"res_codon"});
               push(@multi_mut_all, $known_mutations{$gene}{$pos}{"codons_pos"}) unless (grep { $_ eq $known_mutations{$gene}{$pos}{"codons_pos"}} @multi_mut_all);
               $GENES{$gene}{"multi_mut"} = 1;
            }
            elsif (exists $known_mutations{$gene}{$pos}) {
               push(@known_pos, $pos);
               push(@known_codon, $mismatch_codon[$j]);
               push(@res_codon, $known_mutations{$gene}{$pos}{"res_codon"});              
            }
            else {
               push(@unkmown_pos, $pos);
               push(@unknown_codon, $mismatch_codon[$j]);
               push(@ori_codon, $original_codon[$j]);
            }
            $j++;
         }
         
         #Appending for multiple mutations
         if ($GENES{$gene}{"multi_mut"} == 1) {
            push(@known_pos, join(",", @multi_mut_pos));
            push(@known_codon, join(",", @multi_mut_codon));
         }
         
         
         #Saving mutations
         @{$GENES{$gene}{"known_mutations_pos"}} = @known_pos;
         @{$GENES{$gene}{"known_mutations_codon"}} = @known_codon;
         @{$GENES{$gene}{"known_resistance_codon"}} = @res_codon;
         @{$GENES{$gene}{"unknown_mutations_pos"}} = @unkmown_pos;
         @{$GENES{$gene}{"unknown_mutations_codon"}} = @unknown_codon;
         @{$GENES{$gene}{"unknown_original_codon"}} = @ori_codon;
            
         #Saving extra for multiple mutations
         #@{$GENES{$gene}{"known_multi_mutations_pos"}} = @multi_mut_pos;
         #@{$GENES{$gene}{"known_multi_mutations_codon"}} = @multi_mut_codon;
         @{$GENES{$gene}{"known_multi_mutations_ori_codon"}} = @multi_ori_codon;
         @{$GENES{$gene}{"known_multi_resistance_codon"}} = @multi_res_codon;
         @{$GENES{$gene}{"known_multi_mutations_all"}} = @multi_mut_all;
      }
   }  

   # Saving results for printing
   # Counting genes

   my $gene_count = -1;
   foreach my $gene (keys %GENES){
      $gene_count ++;
      #Finding the name of the gene
      my @tmp = split("_", $gene);
      my $gene_name = $tmp[0];
      
      #Printing the results from the gene into the files with mutation informations
      my $found = $GENES{$gene}{'found'};
      my $name = $known_mutations{$gene}{"gen_name"};
      my $cov = sprintf("%.2f", $GENES{$gene}{'perc_cov'});
      
      # Varibles for printing
      $GENE_RESULTS_HASH{$gene} = ();
      $multi_print{$gene} = ();
      $single_print{$gene} = ();
      $silent_mut{$gene} = ();
      $non_silent_mut{$gene} = ();
      push(@{$GENE_RESULTS_HASH{$gene}}, $name); # sorter
      push(@{$GENE_RESULTS_HASH{$gene}}, $cov); # sorter
    
      if ($found ne "no") {
         push(@{$GENE_RESULTS_HASH{$gene}}, 1); # sort
         my @known = @{$GENES{$gene}{"known_mutations_pos"}};
         my @unknown = @{$GENES{$gene}{"unknown_mutations_pos"}};
        
         #Printing the results depending on mutations found
         #No mutations
         if (scalar(@known) == 0 and scalar(@unknown) == 0) {
            # No mutation found
            push(@{$GENE_RESULTS_HASH{$gene}}, "No mutations"); # sorter
         }
         #Known mutation
         if (scalar(@known) != 0) {
            # Genes found
            push(@{$GENE_RESULTS_HASH{$gene}}, "Mutations"); # sorter
            #For printing
            my @multi_print;
            my @single_print;
         
            my $i = 0;
            foreach my $pos (@known) {      
               my $codon = $GENES{$gene}{"known_mutations_codon"}[$i];
               my $res = $known_mutations{$gene}{$pos}{"res"};
               chomp $res;
                
               if ($codon =~ m/,/) { # Results for multiple mutations
                  my @codons_original = @{$known_mutations{$gene}{$pos}{"ref_nuc"}};
                  my @AA_original = @{$known_mutations{$gene}{$pos}{"ref_codon"}};
                  my %res_codons = %{$known_mutations{$gene}{$pos}{"res_codon"}};
                  my @positions = split(",", $pos);
                  my @codons = split(",", $codon);
                  my (@codon_print_ref, @codon_print_res, @AA_print_ref, @AA_print_res, @PMID);
                    
                  my $j = 0;
                  foreach my $position (@positions) {
                     my $AA = &translation($codons[$j]);
                     my $org_codon = $codons_original[$j];
                     my $mut_codon = $codons[$j];
                     my $org_AA = $AA_original[$j];
                           
                     if (exists $res_codons{$AA}) {
                        push(@codon_print_ref, "$org_codon");
                        push(@codon_print_res, "$mut_codon");
                        push(@AA_print_ref, "$org_AA");
                        push(@AA_print_res, "$AA");
                        push(@PMID, "$res_codons{$AA}");
                     }
                     elsif ($AA =~ m/^1$/) {
                        push(@codon_print_ref, "$org_codon");
                        push(@codon_print_res, "$mut_codon");
                        push(@AA_print_ref, "$org_AA");
                        push(@AA_print_res, "DEL") unless $AA_print_res[-1] eq "DEL";
                        
                        if (exists $res_codons{"del"}) {
                          push(@PMID, $res_codons{"del"});
                        }
                        else {
                          push(@PMID, "No");
                        }
                     }
                     else {
                        push(@codon_print_ref, "$org_codon");
                        push(@codon_print_res, "$mut_codon");
                        push(@AA_print_ref, "$org_AA");
                        push(@AA_print_res, "$AA");
                        push(@PMID, "No");
                        $res = "Unknown";
                     }
                     $j++;
                  }
                  my $txt = "$pos\t". join(", ",@codon_print_ref) . "\t",join(", ",@codon_print_res) . "\t".  join(", ",@AA_print_ref) . "\t", join(", ",@AA_print_res) ."\t$res\t" . join(", ",@PMID);
                  #print "text: $txt\n";
                  push(@{$single_print{$gene}}, $txt);
               }
               else { #Results for single mutations
                  my $AA;
                  my %res_codon = %{$known_mutations{$gene}{$pos}{"res_codon"}};
                  my $codon_original = $known_mutations{$gene}{$pos}{"ref_nuc"};
                  
                  if (grep { $_ eq $gene} @RNAgenes) {
                     if (exists $res_codon{$AA}) {
                        $res_codon{$codon} = "No"
                     }
                     push(@{$single_print{$gene}},"$pos\t$codon_original\t$codon\t$res\t$res_codon{$codon}");
                  }
                  else {
                     $AA = &translation($codon);
                     my $AA_original = $known_mutations{$gene}{$pos}{"ref_codon"};
                     #print "AA $AA, AA_org: $AA_original, condon $codon_original\n";
                                       
                     if (exists $res_codon{$AA}) { # If mutation == known mutations
                        push(@{$single_print{$gene}},"$pos\t$codon_original\t$codon\t$AA_original\t$AA\t$res\t$res_codon{$AA}");
                     }
                     elsif ($AA eq $AA_original) {
                        push(@{$single_print{$gene}},"$pos\t$codon_original\t$codon\t$AA_original\t$AA\tSilent mutation\tNo");
                     } 
                     elsif ($AA =~ m/^1$/) {
                        if (exists $res_codon{'del'}) {
                           push(@{$single_print{$gene}},"$pos\t$codon_original\t$codon\t$AA_original\tDEL\t$res\t$res_codon{'del'}");
                        }
                        else {
                           push(@{$single_print{$gene}},"$pos\t$codon_original\t$codon\t$AA_original\tDEL\t$res\tNo");
                        }
                     }  
                     else{ # If mutation != (ne) known mutation
                        push(@{$single_print{$gene}},"$pos\t$codon_original\t$codon\t$AA_original\t$AA\tUnknown\tNo");
                     }
                  }
               }
               $i++;
            }
         }
         else {
           push(@{$GENE_RESULTS_HASH{$gene}}, "No known Mutations"); # sorter
         }
         #Unknown mutations
         if (scalar(@unknown) != 0) {
            #For printing
            push(@{$GENE_RESULTS_HASH{$gene}}, "Unknown Mutations"); # sorter

            my $i = 0;
            foreach my $pos (@unknown) {
               my $codon_mut = $GENES{$gene}{"unknown_mutations_codon"}[$i];
               my $codon_original = $GENES{$gene}{"unknown_original_codon"}[$i];
               my ($AA_mut,$AA_original) ;
               if (grep { $_ eq $gene} @RNAgenes) {
                  unless ($codon_original eq $codon_mut) {
                     push(@{$non_silent_mut{$gene}},"$pos\t$codon_original\t$codon_mut\n");
                 }
               }
               else {
                  $AA_mut = &translation($codon_mut);
                  $AA_original = &translation($codon_original);
                  if ($AA_original eq $AA_mut) { #silent mutations
                      if ($AA_mut =~ m/^1$/) {
                        if ($AA_original =~ m/^1$/) {
                            push(@{$silent_mut{$gene}}, "$pos\t$codon_original\t$codon_mut\tnot known*\tnot known*\n");
                        }
                        else {
                            push(@{$silent_mut{$gene}}, "$pos\t$codon_original\t$codon_mut\t$AA_original\tDEL\n");
                        }
                      }
                      else {
                        if ($AA_original =~ m/^1$/) {
                            push(@{$silent_mut{$gene}}, "$pos\t$codon_original\t$codon_mut\tINS\t$AA_mut\n");
                        }
                        else {
                            push(@{$silent_mut{$gene}}, "$pos\t$codon_original\t$codon_mut\t$AA_original\t$AA_mut\n");
                        }
                      }
                  }
                  else { #non-silent mutations
                      if ($AA_mut =~ m/^1$/) {
                        if ($AA_original =~ m/^1$/) {
                            push(@{$non_silent_mut{$gene}}, "$pos\t$codon_original\t$codon_mut\tnot known*\tnot known*\n");
                        }
                        else {
                            push(@{$non_silent_mut{$gene}}, "$pos\t$codon_original\t$codon_mut\t$AA_original\tDEL\n");
                        }
                      }
                      else {
                        if ($AA_original =~ m/^1$/) {
                            push(@{$non_silent_mut{$gene}}, "$pos\t$codon_original\t$codon_mut\tINS\t$AA_mut\n");
                        }
                        else {
                            push(@{$non_silent_mut{$gene}}, "$pos\t$codon_original\t$codon_mut\t$AA_original\t$AA_mut\n");
                        }
                      }
                  }
               }
               $i++;
            }
            if (!$non_silent_mut{$gene}) {
               push(@{$non_silent_mut{$gene}}, "No non silent unknown mutations\n");
            }
            if (!$silent_mut{$gene}) {
               push(@{$silent_mut{$gene}}, "No silent unknown mutations\n");
            }
         }    
      }
      else {
         push(@{$GENE_RESULTS_HASH{$gene}}, 0); # sort
         if ($frame_shift eq "off" and exists $GENES{$gene}{'frame_warning'}) {
            push(@{$GENE_RESULTS_HASH{$gene}}, "No off");
            #print "$gene is not found due to a introduced frame shift. If you want to see the found ","sequence rerun the program with 'Include frame shifts' on\nName: $name\nCoverage: $cov\n\n";
         }
         else {
            push(@{$GENE_RESULTS_HASH{$gene}}, "No gene");
            #print "$gene is not found\nName: $name\nCoverage: $cov\n\n"
         }     
      }
   }
   #making result files
   push(@RESULTS_AND_SETTINGS_ARRAY, $argfProfiles{$species}); # species
   push(@RESULTS_AND_SETTINGS_ARRAY, $InFile); # Name of input file
   push(@RESULTS_AND_SETTINGS_ARRAY, $threshold); # threshold??
   push(@RESULTS_AND_SETTINGS_ARRAY, $frame_shift); # Frame shift no/off
   push(@RESULTS_AND_SETTINGS_ARRAY, $gene_count); # Frame shift no/off
   
  make_output_files(\@RESULTS_AND_SETTINGS_ARRAY, \%GENE_RESULTS_HASH, \%single_print, \%multi_print, \%non_silent_mut, $patient, $unknown_mutations, @RNAgenes);
}

#Remove temporary blast dir
system("rm -r $PATH/");

exit;



# --------------------------------------------------------------------
# %% Subroutines %%
#
###################################
# Run blast and parse output
# Arguments should be a hash with arguments to blast in option => value format
# Returns text lines of blast output

sub blast {
   my ($input, $PATH) = @_;
   my @genes;
   open(IN,'<'. "$DB/genes.txt") or die "Can't open gene file";
   while (defined(my $line = <IN>)) {
      chomp($line);
      push(@genes, $line)
   }
   close IN;
   my %genes;
   my ($qseqid, $sseqid, $slen, $qlen, $length, $sstart, $send, $qstart, $qend, $pident, $qcovhsp, $mismatch, $gaps, $evalue, $qseq, $sseq);
   my $hit_count = 0;
   
   foreach my $gene (@genes) {
      system("$BLASTN -subject $DB/$gene.fsa -query $input -out $PATH/tmp_$gene -outfmt '6 sseqid qseqid slen qlen length sstart send qstart qend pident qcovhsp mismatch gaps evalue qseq sseq' -max_target_seqs 1");
      if (-z "$PATH/tmp_$gene") {
         $genes{$gene}{1}{"qseq"} = "";              
         $genes{$gene}{1}{"sseq"} = "";
         $genes{$gene}{1}{"len"} = 0;
         $genes{$gene}{1}{"slen"} = 0;
         $genes{$gene}{1}{"qlen"} = 0;
         $genes{$gene}{1}{"qpos_start"} = 0;
         $genes{$gene}{1}{"qpos_end"} = 0;
         $genes{$gene}{1}{"spos_start"} = 0;
         $genes{$gene}{1}{"spos_end"} = 0;
         $genes{$gene}{1}{"pident"} = 0;
         $genes{$gene}{1}{"covhsp"} = 0;
         $genes{$gene}{1}{"mismatch"} = 0;
         $genes{$gene}{1}{"gaps"} = 0;
         $genes{$gene}{1}{"evalue"} = 0;
      }
      else {
         open(IN, '<', "$PATH/tmp_$gene") or die "Can't find BLAST file";
         
         while (defined(my $line = <IN>)) {
            my @blast = split("\t", $line);
            $qseqid = $blast[0];
            $sseqid = $blast[1];
            $slen = $blast[2];
            $qlen = $blast[3];
            $length = $blast[4];
            $sstart = $blast[5];
            $send = $blast[6];
            $qstart = $blast[7];
            $qend = $blast[8];
            $pident = $blast[9];
            $qcovhsp = $blast[10];
            $mismatch = $blast[11];
            $gaps = $blast[12];
            $evalue = $blast[13];
            $qseq = $blast[14];
            $sseq = $blast[15];
            
            if ($pident > '80') {
               $hit_count++;
               $genes{$gene}{$hit_count}{"qseq"} = $qseq;              
               $genes{$gene}{$hit_count}{"sseq"} = $sseq;
               $genes{$gene}{$hit_count}{"len"} = $length;
               $genes{$gene}{$hit_count}{"slen"} = $slen;
               $genes{$gene}{$hit_count}{"qlen"} = $qlen;
               $genes{$gene}{$hit_count}{"qpos_start"} = $qstart;
               $genes{$gene}{$hit_count}{"qpos_end"} = $qend;
               $genes{$gene}{$hit_count}{"spos_start"} = $sstart;
               $genes{$gene}{$hit_count}{"spos_end"} = $send;
               $genes{$gene}{$hit_count}{"pident"} = $pident;
               $genes{$gene}{$hit_count}{"covhsp"} = $qcovhsp;
               $genes{$gene}{$hit_count}{"mismatch"} = $mismatch;
               $genes{$gene}{$hit_count}{"gaps"} = $gaps;
               $genes{$gene}{$hit_count}{"evalue"} = $evalue;
            }
         }
         close IN;
      }
      #print "Gene: $gene, $qcovhsp, $pident, $mismatch\n";
      #print "$qseq\n";
      #print "$sseq\n";
   }
   return %genes;
}

#####################################
sub mutations {
    open(IN,"<", $drug) or die "Can't find resistens look-up file";
    my $Header = <IN>;
    my %known_mutations;
    
    while (defined(my $line = <IN>)) {
        chomp($line);
        my @mutations = split("\t", $line);
        my $gene_acc = $mutations[0];
        my @tmp_codon = split(',', $mutations[6]); #keys
        my @tmp_PMID = split(',', $mutations[8]); #value
        my %res_codons;
        
        for (my $i = 0; $i < scalar(@tmp_codon); $i++) {
          $res_codons{$tmp_codon[$i]} = $tmp_PMID[$i];
        }
        
        #@res_codons{@tmp_codon} = @tmp_PMID;
        #my %res_codons = map{$_ => 1} @tmp_res;
        
        my $no_mutations = $mutations[2];
        if ($no_mutations > 1) {
            my @codons_pos = split(",", $mutations[3]);
            my @ref_nucs = split(",", $mutations[4]);
            my @ref_codons = split(",", $mutations[5]);
            #Makring Resistance codon hash
            my @res_codons = split(",", $mutations[6]);
               
            #Getting the information needed to find each mutation
            for (my $count = 0; $count < $no_mutations; $count ++) {
                #Getting the information for each mutations
                $known_mutations{$gene_acc}{$codons_pos[$count]}{"no_mut"} = $no_mutations;
                $known_mutations{$gene_acc}{$codons_pos[$count]}{"codons_pos"} = $mutations[3];
            }
            
            #saving the final known mutation for resistance
            my $codon_pos = $mutations[3];
            @{$known_mutations{$gene_acc}{$codon_pos}{"ref_nuc"}} = @ref_nucs;
            @{$known_mutations{$gene_acc}{$codon_pos}{"ref_codon"}} = @ref_codons;
            $known_mutations{$gene_acc}{$codon_pos}{"res_codon"} = \%res_codons;
            $known_mutations{$gene_acc}{$codon_pos}{"res"} = $mutations[7];
        }
        else {
            my $codon_pos = $mutations[3];
            #Getting the gene name, ref nucleotide, ref codon and resistens for each know mutations for each gen
            $known_mutations{$gene_acc}{$codon_pos}{"ref_nuc"} = $mutations[4];
            $known_mutations{$gene_acc}{$codon_pos}{"ref_codon"} = $mutations[5];
            $known_mutations{$gene_acc}{$codon_pos}{"res_codon"} = \%res_codons;
            $known_mutations{$gene_acc}{$codon_pos}{"res"} = $mutations[7];
        }
        
        #Saving the gene name
        $known_mutations{$gene_acc}{"gen_name"} = $mutations[1];
    }
    
    close IN;
    
    return %known_mutations;
}
#####################################
#The subroutine that translate the codon
sub translation {
   my ($codon, $stop_off) = @_;
   my %codons = (ATT => 'I', ATC => 'I', ATA => 'I', CTT => 'L', CTC => 'L', CTA => 'L', CTG => 'L', TTA => 'L', TTG => 'L', GTT => 'V',
               GTC => 'V', GTA => 'V', GTG => 'V', TTT => 'F', TTC => 'F', ATG => 'M', TGT => 'C', TGC => 'C', GCT => 'A', GCC => 'A',
               GCA => 'A', GCG => 'A', GGT => 'G', GGC => 'G', GGA => 'G', GGG => 'G', CCT => 'P', CCC => 'P', CCA => 'P', CCG => 'P',
               ACT => 'T', ACC => 'T', ACA => 'T', ACG => 'T', TCT => 'S', TCC => 'S', TCA => 'S', TCG => 'S', AGT => 'S', AGC => 'S',
               TAT => 'Y', TAC => 'Y', TGG => 'W', CAA => 'Q', CAG => 'Q', AAT => 'N', AAC => 'N', CAT => 'H', CAC => 'H', GAA => 'E',
               GAG => 'E', GAT => 'D', GAC => 'D', AAA => 'K', AAG => 'K', CGT => 'R', CGC => 'R', CGA => 'R', CGG => 'R', AGA => 'R',
               AGG => 'R', TAA => 'STOP', TAG => 'STOP', TGA => 'STOP');
   
   my %codons_no_stop = (ATT => 'I', ATC => 'I', ATA => 'I', CTT => 'L', CTC => 'L', CTA => 'L', CTG => 'L', TTA => 'L', TTG => 'L', GTT => 'V',
               GTC => 'V', GTA => 'V', GTG => 'V', TTT => 'F', TTC => 'F', ATG => 'M', TGT => 'C', TGC => 'C', GCT => 'A', GCC => 'A',
               GCA => 'A', GCG => 'A', GGT => 'G', GGC => 'G', GGA => 'G', GGG => 'G', CCT => 'P', CCC => 'P', CCA => 'P', CCG => 'P',
               ACT => 'T', ACC => 'T', ACA => 'T', ACG => 'T', TCT => 'S', TCC => 'S', TCA => 'S', TCG => 'S', AGT => 'S', AGC => 'S',
               TAT => 'Y', TAC => 'Y', TGG => 'W', CAA => 'Q', CAG => 'Q', AAT => 'N', AAC => 'N', CAT => 'H', CAC => 'H', GAA => 'E',
               GAG => 'E', GAT => 'D', GAC => 'D', AAA => 'K', AAG => 'K', CGT => 'R', CGC => 'R', CGA => 'R', CGG => 'R', AGA => 'R',
               AGG => 'R', TAA => '', TAG => '', TGA => '');
    
    if ($codon =~ m/^[ATCGatcg]{3}$/) {
        return $codons_no_stop{$codon} if defined($stop_off);
        return $codons{$codon} unless defined($stop_off);
    }
    if ($codon =~ m/[Nn]/) {
        my $unknown = "-";
        return $unknown;
    }
    
    else {
        return "1";
    }
}
#####################################
sub multi_mutations {
    my ($gene, $patient, $PATH, %genes) = @_;
    my @positions = @{$genes{$gene}{"known_multi_mutations_pos"}};
    my @codons = @{$genes{$gene}{"known_multi_mutations_codon"}};
    my @orginal_codons = @{$genes{$gene}{"known_multi_mutations_ori_codon"}};
    my @all_mut = @{$genes{$gene}{"known_multi_mutations_all"}};
    
    foreach my $multi_mut (@all_mut) {
        my @mutations = split(",", $multi_mut);
        my $no_mut_found = 0;
        foreach my $pos (@positions) {
            $no_mut_found++ if (grep { $_ eq $pos} @mutations);
        }
        
        #If all the mutations are found the result is saved under known mutations 
        if ($no_mut_found == scalar(@mutations)) {
            push(@{$genes{$gene}{"known_mutations_pos"}}, join(",", @positions));
            push(@{$genes{$gene}{"known_mutations_codon"}}, join(",", @codons));
        }
        #If some of the mutations are missing the result are saved under unknown mutations
        else {
            my $res = $known_mutations{$gene}{join(",", @all_mut)}{"res"};
            chomp $res;
            my $pos = join(",", @positions);
            push(@{$genes{$gene}{"unknown_mutations_pos"}}, $pos);
            push(@{$genes{$gene}{"unknown_mutations_codon"}}, join(",", @codons));
            push(@{$genes{$gene}{"unknown_original_codon"}}, join(",", @orginal_codons));
            $genes{$gene}{$pos}{"res_message"} = "*This is a unknown mutation! Required mutation positions for resistance against $res is: ". join(",", @all_mut ). "\n";
        }
    }
}
# --------------------------------------------------------------------
# %% Help Page/Documentation %%
#

sub print_help {
  my $ProgName     = PROGRAM_NAME;
  my $ProgNameLong = PROGRAM_NAME_LONG;
  my $Version      = VERSION;
  my $CMD = join(" ", %ARGV);
  print <<EOH;

NAME
  $ProgName - $ProgNameLong

SYNOPSIS
  $ProgName -S [species] [Options] < [File]
     or
  $ProgName -S [SPECIES] -i [File] [Options]

DESCRIPTION
  Calculates antibiotic resistance genes based on a BLAST alignment of the input
  sequence file and the specified allele set. If possible the ST will be
  given, or if unknown, that field will be left empty

        Notice that although the options mimic that the input sequences are
  aligned against the alleles, it is in fact the other way around. First,
  the input is converted to a blast database, against which is aligned the
  alleles from the species specified with '-d'.

  Notice also that the default options for BLAST are changed to suit the
  ResFinder alignment. Although the user can change the arguments just as if he
  was running blastall directly, this is the command line used as default
  in this script:

         $CMD

  These defaults can be overridden by giving them as arguments to this
  script, although it will likely break if anything other than '-a' is
  changed. Example:

  $ProgName -a 10 -i [File] -S [species]

OPTIONS
  Most options are simply forwarded to the call to BLAST. A few are unique
  to this script and a few blast arguments deserve special mention.
   
      -i [file]
   Input file
   
     -S [species]
  The species for the input gene.
  These are in fact the core names of a .fsa located in the database directory:

  $DB

     -I [Format]
  Specify the sequence format of the input file.
  If the input file is in another format than fasta, this can be specified
  here. Most BioPerl sequence formats are supported.
  Default is 'fasta'

     -O [Format]
  Specifies the format of the output.
  If left unspecified (the default), the ST is given along with the
  corresponding allele numbers in a tabbed format. If set to a sequence
  format, e.g. 'tab' or 'fasta', the sequence of the alleles will instead
  be outputted in the requested format.

     -h or --help
  Prints this text.

VERSION
    Current: $Version

AUTHORS
    Rosa Alles¿e, rosa\@cbs.dtu.dk, Ea Zankari, ea\@cbs.dtu.dk

EOH
}

########### Html ###########
sub make_output_files{
   my ($resultsAndSettingsArray, $geneResultsHash, $singleHash, $multiHash, $nonsilentHash, $patient, $unknown_mutations, @RNAgenes) = @_;
   open(TABLE, '>', $dir. '/point_results_table.txt') or die "Could not create result file";
   open(TAB_KNOWN, '>', $dir.'/point_results_tab_known.txt') or die "Could not create result file";
   #open(TAB_RNA, '>', $dir.'/point_results_tab_RNA.txt') or die "Could not create result file";
   if ($unknown_mutations) {
     open(TAB_UNKNOWN, '>', $dir.'/point_results_tab_unknown.txt') or die "Could not create result file";
   }
   #printing results
   print TABLE "Chromosomale point mutations - Results\n";
   print TABLE "Species: ".@{$resultsAndSettingsArray}[0]."\n\n";
   print TAB_KNOWN "Mutation\tCodon or nucleotide change\tAmino acid change\tResistance\tPMID\n";
   #print TAB_RNA "Mutation\tNucleotide change\tResistance\tPMID\n";
   if ($unknown_mutations) {
     print TAB_UNKNOWN "Mutation\tCodon change\tAmino acid change\n";
   }
  
   # Results table for each gene
   print TABLE "Known mutations\n\n";
   foreach my $sort (sort (keys %{$geneResultsHash})){
      #Open table
      my $array = ${$geneResultsHash}{$sort}; # @$array = antal
      my $gene_name = @$array[0];
      print TABLE "$gene_name\n";
      
      
      if (@$array[2] eq 0) {
         if (@$array[3] eq "No off") {
            print TABLE @$array[0]." is not found due to a introduced frame shift. If you want to see the found ",
            "sequence rerun the program with 'Include frame shifts' on. Coverage is: ".@$array[1]."\n";
         }
         elsif (@$array[3] eq "No gene") {
            print TABLE @$array[0]." is not found.\n";
         }
         print TABLE"\n"; # close table
      }
      else{
         my $single = ${$singleHash}{$sort};
         my $nonsilent = ${$nonsilentHash}{$sort};
         
         if (grep { $_ eq "No mutations"} @$array) {
            print TABLE "No mutations found in gene in $gene_name\n";
         }
         elsif (grep { $_ eq "No known Mutations"} @$array) {
            print TABLE "No known mutations in $gene_name\n";
         }
         if (grep { $_ eq "Mutations"} @$array) {
            if (grep { $_ eq $sort} @RNAgenes) {
              print TABLE "Mutation\tNucleotide change\tResistance\tPMID\n";
              for (my $i = 0; $i < scalar(@$single); $i++){
                my @tmp = split("\t",@$single[$i]);
                chomp($tmp[4]);
                if ($tmp[1] eq $tmp[2]) {
                    print TABLE "$gene_name\t$tmp[1]$tmp[0]$tmp[2]\t$tmp[1] -> $tmp[2]\t",
                    "\tSilent mutation\t$tmp[4]\n";
                    print TAB_KNOWN "$gene_name\,$tmp[1]$tmp[0]$tmp[2]\t$tmp[1] -> $tmp[2]\tNA\t",
                    "\tSilent mutation\t$tmp[4]\n";
                }
                else {
                    print TAB_KNOWN "$gene_name\,$tmp[1]$tmp[0]$tmp[2]\t$tmp[1] -> $tmp[2]\tNA\t",
                    "$tmp[3]\t$tmp[4]\n";
                    print TABLE "$gene_name\t$tmp[1]$tmp[0]$tmp[2]\t$tmp[1] -> $tmp[2]\t",
                    "$tmp[3]\t$tmp[4]\n";
                }
              }
            }
            else {
              print TABLE "Mutation\tCodon change\tAmino acid change\tResistance\tPMID\n";
              for (my $i = 0; $i < scalar(@$single); $i++){
                #print @$single[$i] . "\n";
                my @tmp = split("\t",@$single[$i]);
                #print @tmp . "\n";
                chomp($tmp[6]);
                if ($tmp[3] eq $tmp[4]) {
                    print TABLE "$gene_name\,$tmp[3]$tmp[0]$tmp[4]\t$tmp[1] -> $tmp[2]\t",
                    "$tmp[3] -> $tmp[4]\tSilent mutation\t$tmp[6]\n";
                    print TAB_KNOWN "$gene_name\,$tmp[3]$tmp[0]$tmp[4]\t$tmp[1] -> $tmp[2]\t",
                    "$tmp[3] -> $tmp[4]\tSilent mutation\t$tmp[6]\n";
                }
                else {
                    print TABLE "$gene_name\,$tmp[3]$tmp[0]$tmp[4]\t$tmp[1] -> $tmp[2]\t",
                    "$tmp[3] -> $tmp[4]\t$tmp[5]\t$tmp[6]\n";
                    print TAB_KNOWN "$gene_name\,$tmp[3]$tmp[0]$tmp[4]\t$tmp[1] -> $tmp[2]\t",
                    "$tmp[3] -> $tmp[4]\t$tmp[5]\t$tmp[6]\n";
                }
              }
            }
         }
         print TABLE "\n"; # close table
      }
   }
   if ($unknown_mutations) {
    print TABLE "Unknown mutations\n\n";
    foreach my $sort (sort (keys %{$geneResultsHash})){
      #Open table
      my $array = ${$geneResultsHash}{$sort}; # @$array = antal 
      my $gene_name = @$array[0];
      
      print TABLE "$gene_name\n";
      if (@$array[2] eq 0) {
         if (@$array[3] eq "No off") {
            print TABLE @$array[0]." is not found due to a introduced frame shift. If you want to see the found ",
            "sequence rerun the program with 'Include frame shifts' on. Coverage is: ".@$array[1]."\n";
         }
         elsif (@$array[3] eq "No gene") {
           print TABLE @$array[0]." is not found.\n";
         }
         print TABLE"\n"; # close table
      }
      else{
         my $nonsilent = ${$nonsilentHash}{$sort};
         if (grep { $_ eq "Unknown Mutations"} @$array){
             if (grep { $_ eq $sort} @RNAgenes) {
               print TABLE "Mutation\tNucleotide change\n";
               for (my $i = 0; $i < scalar(@$nonsilent); $i++){
                 my @tmp = split("\t",@$nonsilent[$i]);
                 chomp($tmp[2]);
                 
                 print TABLE "$gene_name\t$tmp[1]$tmp[0]$tmp[2]\t$tmp[1] -> $tmp[2]\n";
               }
             }
             else {
               print TABLE "Mutation\tCodon change\tAmino acid change\n";
               for (my $i = 0; $i < scalar(@$nonsilent); $i++){
                 my @tmp = split("\t",@$nonsilent[$i]);
                 chomp($tmp[4]);
                 print TABLE "$gene_name\t$tmp[3]$tmp[0]$tmp[4]\t$tmp[1] -> $tmp[2]\t",
                     "$tmp[3] -> $tmp[4]\n";
                     
                 print TAB_UNKNOWN "$gene_name\t$tmp[3]$tmp[0]$tmp[4]\t$tmp[1] -> $tmp[2]\t",
                     "$tmp[3] -> $tmp[4]\n";
                 }
             }
         }
      }
      print TABLE "\n";
    }
   }
   # END OF TABLE
} # make_output_files :: END
